package com.company;

import com.company.data.Generator;
import com.company.model.Shop;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Shop[] shops = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите название магазина для поиска в нем самого дешевого смарфона: ");
        String inputNameShopForCheapPhone = scan.nextLine();

        for (Shop shop : shops) {
            if (shop.hasNameShop(inputNameShopForCheapPhone)) {
                shop.findCheapPhone();
                shop.outputInfoShop();
            }
        }

        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("Введите название магазина для поиска в нем смартфона определенного бренда: ");
        String inputNameShopForBrand = scan.nextLine();
        System.out.println("Введите название бренда: ");
        String inputNameBrand = scan.nextLine();

        for (Shop shop : shops) {
            if (shop.hasNameShop(inputNameShopForBrand)) {
                shop.outputPhoneOfBrand(inputNameBrand);
            }
        }

        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("Введите название магазина для поиска в нем смартфонов в ценовом интервале: ");
        String inputNameShopForMinMaxPricePhones = scan.nextLine();
        System.out.println("Введите минимальную цену: ");
        int minPrice = Integer.parseInt(scan.nextLine());
        System.out.println("Введите максимальную цену: ");
        int maxPrice = Integer.parseInt(scan.nextLine());

        for (Shop shop : shops) {
            if (shop.hasNameShop(inputNameShopForMinMaxPricePhones)) {
                shop.outputMinMaxPricePhones(minPrice, maxPrice);
                shop.outputInfoShop();
            }
        }

        System.out.println("---------------------------------------------------------------------------------");
        int checkPriceForAllShops = Integer.MAX_VALUE;
        for (Shop shop : shops) {
            if (shop.findCheapPhoneForReturnPrice()<checkPriceForAllShops){
                checkPriceForAllShops = shop.findCheapPhoneForReturnPrice();
            }
        }
        for (Shop shop : shops) {
            shop.outputCheapPhoneForAllShop(checkPriceForAllShops);
        }

        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("Введите название бренда для поиска всех телефонов по всем магазинам: ");
        String inputNameBrandForAllShops = scan.nextLine();

        for (Shop shop : shops) {
            shop.outputPhoneOfBrand(inputNameBrandForAllShops);
        }

        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("Введите название смартфона (бренд или модель) для поиска самого дешевого по всем магазинам: ");
        String inputModelOrBrandPhone = scan.nextLine();
        int checkPriceForBrandModel = Integer.MAX_VALUE;
        for (Shop shop : shops) {
            if (shop.findPhoneOfBrandModelForReturnPrice(inputModelOrBrandPhone)<checkPriceForBrandModel){
                checkPriceForBrandModel = shop.findPhoneOfBrandModelForReturnPrice(inputModelOrBrandPhone);
            }
        }
        for (Shop shop : shops) {
            shop.outputCheapPhoneForBrandModel(checkPriceForBrandModel);
        }

    }
}
