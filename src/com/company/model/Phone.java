package com.company.model;

public class Phone {
    private String brand;
    private String model;
    private int price;

    public Phone(String brand, String model, int price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Boolean hasNameBrand(String name){
        return getBrand().toLowerCase().contains(name.toLowerCase());
    }

    public Boolean hasModel(String model){
        return getModel().toLowerCase().contains(model.toLowerCase());
    }

}
