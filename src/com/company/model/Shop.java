package com.company.model;

public class Shop {
    private String name;
    private String address;
    private Phone[] phones;

    public Shop(String name, String address, Phone[] phones) {
        this.name = name;
        this.address = address;
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Boolean hasNameShop(String name) {
        return getName().toLowerCase().contains(name.toLowerCase());
    }

    public void findCheapPhone() {
        int idCheapPhone = 0;
        int priceCheapPhone = Integer.MAX_VALUE;
        for (int i = 0; i < phones.length; i++) {
            if (phones[i].getPrice() < priceCheapPhone) {
                priceCheapPhone = phones[i].getPrice();
                idCheapPhone = i;
            }
        }
        outputInfoPhone(idCheapPhone);
    }

    public void outputPhoneOfBrand(String nameBrand) {
        for (Phone phone : phones) {
            if (phone.hasNameBrand(nameBrand)) {
                System.out.println("Смартфон: " + phone.getBrand() + " " + phone.getModel() + ". Его цена: " + phone.getPrice());
                outputInfoShop();
            }
        }
    }

    public void outputMinMaxPricePhones(int min, int max) {
        for (Phone phone : phones) {
            if (phone.getPrice() > min && phone.getPrice() < max) {
                System.out.println("Смартфон: " + phone.getBrand() + " " + phone.getModel() + ". Его цена: " + phone.getPrice());
            }
        }
    }

    public int findCheapPhoneForReturnPrice() {
        int priceCheapPhone = Integer.MAX_VALUE;
        for (int i = 0; i < phones.length; i++) {
            if (phones[i].getPrice() < priceCheapPhone) {
                priceCheapPhone = phones[i].getPrice();
            }
        }
        return priceCheapPhone;
    }

    public void outputCheapPhoneForAllShop(int check) {
        for (Phone phone : phones) {
            if (phone.getPrice() == check) {
                System.out.println("Самый дешевый смартфон во всех магазинах: " + phone.getBrand() + " " + phone.getModel() + ". Его цена: " + phone.getPrice());
                outputInfoShop();
            }
        }
    }

    public int findPhoneOfBrandModelForReturnPrice(String nameBrandModel) {
        int priceBrandModelPhone = Integer.MAX_VALUE;
        for (int i = 0; i < phones.length; i++) {
            if (phones[i].hasNameBrand(nameBrandModel) || phones[i].hasModel(nameBrandModel)) {
                if (phones[i].getPrice() < priceBrandModelPhone) {
                    priceBrandModelPhone = phones[i].getPrice();
                }
            }
        }
        return priceBrandModelPhone;
    }

    public void outputCheapPhoneForBrandModel(int check) {
        for (Phone phone : phones) {
            if (phone.getPrice() == check) {
                System.out.println("Самый дешевый смартфон: " + phone.getBrand() + " " + phone.getModel() + ". Его цена: " + phone.getPrice());
                outputInfoShop();
            }
        }
    }

    public void outputInfoShop() {
        System.out.println("Название магазина: <<" + getName() + ">>. Его адресс: " + getAddress());
    }

    public void outputInfoPhone(int id) {
        System.out.println("Смартфон: " + phones[id].getBrand() + " " + phones[id].getModel() + ". Его цена: " + phones[id].getPrice());
    }
}
