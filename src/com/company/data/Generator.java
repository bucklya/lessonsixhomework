package com.company.data;

import com.company.model.Phone;
import com.company.model.Shop;


public final class Generator {
    private Generator() {
    }

    public static Shop[] generate() {
        Shop[] shops = new Shop[5];
        {
            Phone[] phones = {
                    new Phone("Xiaomi", "Mi Max 3", 4699),
                    new Phone("Samsung", "Galaxy Note 10 SM", 19500),
                    new Phone("Apple", "IPhone 8", 13760),
                    new Phone("Meizu", "M10", 3200),
                    new Phone("Huawei", "P40", 6440)
            };
            shops[0] = new Shop("Sota", "Площадь Конституции 2/2, Харьков", phones);
        }

        {
            Phone[] phones = {
                    new Phone("Xiaomi", "Mi Max 3", 4799),
                    new Phone("Samsung", "Galaxy Note 10 SM", 19015),
                    new Phone("Huawei", "P40", 6450)
            };
            shops[1] = new Shop("Allo", "Площадь Конституции 1, Харьков", phones);
        }

        {
            Phone[] phones = {
                    new Phone("Xiaomi", "Mi Max 3", 4700),
                    new Phone("Apple", "IPhone 8", 13800),
                    new Phone("Meizu", "M10", 3190),
                    new Phone("Huawei", "P40", 6500)
            };
            shops[2] = new Shop("Citrus", "ул. Б. Васильковская, 66/68, Харьков", phones);
        }

        {
            Phone[] phones = {
                    new Phone("Apple", "IPhone 8", 13699),
            };
            shops[3] = new Shop("Foxtrot", "ул. Вернадського, 2, Харьков", phones);
        }

        Phone[] phones = {
                new Phone("Samsung", "Galaxy Note 10 SM", 19350),
                new Phone("Meizu", "M10", 3100),
        };
        shops[4] = new Shop("Comfy", "ул. Академіка Павлова, 44-б, Харьков", phones);
        return shops;
    }

}

